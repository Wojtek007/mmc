import React,{ useEffect,useState,useRef } from 'react';
import Plot from 'react-plotly.js';
import logo from './logo.svg';
import rozkladRownomierny from './rozklad_rownomierny'
import './App.css';


function nthroot(x,n) {
  try {
    var negate = n % 2 == 1 && x < 0;
    if(negate)
      x = -x;
    var possible = Math.pow(x,1 / n);
    n = Math.pow(possible,n);
    if(Math.abs(x - n) < 1 && (x > 0 == n > 0))
      return negate ? -possible : possible;
  } catch(e) { }
}

let eliminacjiTime = []
let supremacjiTime = []

function App() {
  const [counter,setCounter] = useState(0);
  const [bucketSize,setBucketSize] = useState([0.1,10]);
  const interval = useRef(false) //now you can pass timer to another component
  const numbersLimit = 26;

  useEffect(() => {
    interval.current = setInterval(() => {
      setCounter(counter => counter < numbersLimit ? counter + 1 : counter);
    },100);

    return () => {
      clearInterval(interval.current);
    };
  },[]);
  const zmniejszonyRozkladRownomierny = rozkladRownomierny.filter((ele,i) => i % 2 === 0)
  const stepRozklad = zmniejszonyRozkladRownomierny.length / numbersLimit;
  const przefiltrowanyRozkladRownomierny = zmniejszonyRozkladRownomierny.filter((tuple,i) => i < stepRozklad * counter)

  const przeskalowanyRozkladRownomierny = przefiltrowanyRozkladRownomierny
    .map(([i,u1,u2]) => {
      const u1out = (u1 * 2);
      const u2out = (u2 * 2);
      return [i,u1out,u2out]
    })

  const f = x => (5 / 12) * (1 + ((x - 1) * (x - 1) * (x - 1) * (x - 1)));

  const t0 = performance.now()

  const metodaEliminacji = przeskalowanyRozkladRownomierny.reduce((acc,[i,u1,u2In]) => {
    const u2 = f(u2In);
    return u2 < f(u1) ? [...acc,u1] : acc;
  },[])

  const t1 = performance.now()

  const fs = x => (5 / 12) + ((5 / 12) * (x - 1) * (x - 1) * (x - 1) * (x - 1));

  eliminacjiTime = [...eliminacjiTime,t1 - t0];

  const t2 = performance.now()

  const metodaSuperpozycji = przeskalowanyRozkladRownomierny.reduce((acc,[i,u1],j) => {
    const u2 = przefiltrowanyRozkladRownomierny[j];
    if(u2 < (5 / 6)) {
      return [...acc,u1];
    } else {
      const res = 1 + nthroot(u1 - 1,5);
      // console.log('sup',res,u1 - 1);
      return [...acc,res];
    }
  },[])
  const t3 = performance.now()
  supremacjiTime = [...supremacjiTime,t3 - t2];

  const createHistogram = (acc,ele) => {
    const bucket = Math.floor(ele / bucketSize[0]);
    return {
      ...acc,
      [bucket]: acc[bucket] ? acc[bucket] + 1 : 1
    }
  }

  const histogramEliminacji = metodaEliminacji.reduce(createHistogram,{})
  const histogramSupremacji = metodaSuperpozycji.reduce(createHistogram,{})

  return (
    <div className="App">
      <header className="App-header">
        MMC lab 3 - Zadanie 3
      </header>

      <span style={{ display: 'inline-block' }}>
        <div>
          <h4>Wielkość kieszonki histogramu</h4>
          <button onClick={() => setBucketSize([0.001,1000])}>0.001</button>
          <button onClick={() => setBucketSize([0.01,100])}>0.01</button>
          <button onClick={() => setBucketSize([0.1,10])}>0.1</button>
          <button onClick={() => setBucketSize([0.2,5])}>0.2</button>
          <button onClick={() => setBucketSize([0.5,2])}>0.5</button>
          <button onClick={() => setBucketSize([1,1])}>1</button>
        </div>
        <div><button onClick={() => { setCounter(0); eliminacjiTime = []; supremacjiTime = []; }}>RESTART</button></div>

        <span style={{ display: 'inline-block' }}>

          <h4>Czas wykonania eliminacji {(t1 - t0).toFixed(4)} ms <br />dla {(stepRozklad * counter).toFixed(0)} elementów </h4>

          <Plot
            data={[
              {
                x: Object.keys(histogramEliminacji).map(key => key / bucketSize[1]),
                y: Object.keys(histogramEliminacji).map(key => histogramEliminacji[key]),
                type: 'bar'
              }
            ]}
            layout={{
              title: 'Metoda eliminacji',
              autosize: false,
              width: 500,
              height: 500,

            }}
          />
        </span>
        <span style={{ display: 'inline-block' }}>

          <h4>Czas wykonania supremacji {(t3 - t2).toFixed(4)} ms <br />dla {(stepRozklad * counter).toFixed(0)} elementów </h4>

          <Plot
            data={[
              {
                x: Object.keys(histogramSupremacji).map(key => key / bucketSize[1]),
                y: Object.keys(histogramSupremacji).map(key => histogramSupremacji[key]),
                type: 'bar'
              }
            ]}
            layout={{
              title: 'Metoda supremacji',
              autosize: false,
              width: 500,
              height: 500,

            }}
          />
        </span>

      </span>








      <span style={{ display: 'inline-block' }}>

        <span style={{ display: 'inline-block' }}>

          <Plot
            data={[
              {
                x: eliminacjiTime.map((a,i) => (i).toFixed(0)),
                y: eliminacjiTime,
                type: 'scatter',
                name: 'Eliminacji'
              },
              {
                x: supremacjiTime.map((a,i) => (i).toFixed(0)),
                y: supremacjiTime,
                type: 'scatter',
                name: 'Supremacji'
              }
            ]}
            layout={{
              title: 'Czasy metod [ms]',
              autosize: true,
              width: 500,
              height: 500,

            }}
          />
        </span>


      </span>
    </div >
  );
}

export default App;
