import React,{ useEffect,useState,useRef } from 'react';
import Plot from 'react-plotly.js';
import logo from './logo.svg';
import rozkladRownomierny from './rozklad_rownomierny'
import './App.css';
import Zad2 from './zad2';
import Zad3 from './zad3';

function App() {
  return (
    <div className="App">
      <Zad2 />
      <Zad3 />

    </div >
  );
}

export default App;
