import React,{ useEffect,useState,useRef } from 'react';
import Plot from 'react-plotly.js';
import logo from './logo.svg';
import rozkladRownomierny from './rozklad_rownomierny'
import './App.css';

let boxTime = []
let marsagliTime = []

function App() {
  const [counter,setCounter] = useState(0);
  const [bucketSize,setBucketSize] = useState([0.1,10]);
  const numbersLimit = 26;
  const rozkladLength = rozkladRownomierny.length
  const stepRozklad = rozkladRownomierny.length / numbersLimit;
  const interval = useRef(false) //now you can pass timer to another component

  useEffect(() => {
    interval.current = setInterval(() => {
      setCounter(counter => counter < numbersLimit ? counter + 1 : counter);
    },100);

    return () => {
      clearInterval(interval.current);
    };
  },[]);

  // useEffect(() => {
  //   console.log(interval,counter,numbersLimit)
  //   if(interval.current && counter >= numbersLimit) {
  //     console.log('KONIEC')
  //     clearInterval(interval.current);
  //   }
  // },[counter]);




  const standardowyNormlany = new Array(numbersLimit).fill(0).map(() => new Array(numbersLimit).fill(0));

  for(let i = 0; i < counter; i++) {
    for(let j = 0; j < counter; j++) {
      // console.log(i,j,-((i * i + j * j) / 2),Math.exp(-((i * i + j * j) / 2)))
      standardowyNormlany[i][j] = Math.exp(-((i * i + j * j) / 2)) / (2 * Math.PI);
    }
  }


  const boxPrzeksztalcenie0 = rozkladRownomierny
    .filter((tuple,i) => i < stepRozklad * counter)

  const t0 = performance.now()
  const boxPrzeksztalcenie1 = boxPrzeksztalcenie0
    .map(([i,u1,u2]) => {
      const x = Math.sqrt(-2 * Math.log(u1)) * Math.cos(2 * Math.PI * u2)
      const y = Math.sqrt(-2 * Math.log(u1)) * Math.sin(2 * Math.PI * u2)
      return [x,y]
    })
  const t1 = performance.now()
  const boxPrzeksztalcenie = boxPrzeksztalcenie1
    .flat()

  boxTime = [...boxTime,t1 - t0];
  const rozkladRownomiernyWIekszy = rozkladRownomierny
    .map(([i,u1,u2]) => {
      const u1out = (u1 * 2) - 1;
      const u2out = (u2 * 2) - 1;
      return [i,u1out,u2out]
    })

  const marsagliPrzeksztalcenie0 = rozkladRownomiernyWIekszy
    .filter((tuple,i) => i < stepRozklad * counter);
  const t2 = performance.now()
  const marsagliPrzeksztalcenie1 = marsagliPrzeksztalcenie0
    .reduce((acc,[i,u1,u2]) => {
      const b = u1 * u1 + u2 * u2;
      if(b > 1) {
        return acc;
      }
      const z = Math.sqrt((-2 * Math.log(b)) / b);
      const x = u1 * z
      const y = u2 * z
      return [...acc,[x,y]]
    },[]);
  const t3 = performance.now()
  const marsagliPrzeksztalcenie = marsagliPrzeksztalcenie1.flat();
  marsagliTime = [...marsagliTime,t3 - t2]

  const createHistogram = (acc,ele) => {
    const bucket = Math.floor(ele / bucketSize[0]);
    return {
      ...acc,
      [bucket]: acc[bucket] ? acc[bucket] + 1 : 1
    }
  }

  const histogramBox = boxPrzeksztalcenie.reduce(createHistogram,{})
  const histogramMarzagli = marsagliPrzeksztalcenie.reduce(createHistogram,{})
  // console.log(boxPrzeksztalcenie)
  // console.log("XXXX",histogramBox)
  return (
    <div className="App">
      <header className="App-header">
        MMC lab 3 - Zadanie 2
      </header>
      <div><button onClick={() => { setCounter(0); boxTime = []; marsagliTime = []; }}>RESTART</button></div>
      <Plot
        data={[
          {
            z: standardowyNormlany,
            type: 'surface',
          }
        ]}
        layout={{
          title: 'Standardowy rozkład normalny dwuwymiarowy',
          autosize: false,
          width: 500,
          height: 500,

        }}
      />
      <span style={{ display: 'inline-block' }}>
        <span style={{ display: 'inline-block' }}>
          <div><button onClick={() => { setCounter(0); boxTime = []; marsagliTime = []; }}>RESTART</button></div>
          <Plot
            data={[
              {
                x: rozkladRownomierny.map(tuple => tuple[1]).filter((tuple,i) => i < stepRozklad * counter),
                y: rozkladRownomierny.map(tuple => tuple[2]).filter((tuple,i) => i < stepRozklad * counter),
                mode: 'markers',
                type: 'scatter'
              }
            ]}
            layout={{
              title: 'Standardowy rozklad równomierny z lab 1',
              autosize: false,
              width: 500,
              height: 500,

            }}
          />
        </span>
        <span style={{ display: 'inline-block' }}>
          <div>
            <h4>Wielkość kieszonki histogramu</h4>
            <button onClick={() => setBucketSize([0.001,1000])}>0.001</button>
            <button onClick={() => setBucketSize([0.01,100])}>0.01</button>
            <button onClick={() => setBucketSize([0.1,10])}>0.1</button>
            <button onClick={() => setBucketSize([0.2,5])}>0.2</button>
            <button onClick={() => setBucketSize([0.5,2])}>0.5</button>
            <button onClick={() => setBucketSize([1,1])}>1</button>
          </div>
          <h4>Czas wtkonania przekształcenia {(t1 - t0).toFixed(4)} ms <br />dla {(stepRozklad * counter).toFixed(0)} elementów </h4>
          <div><button onClick={() => { setCounter(0); boxTime = []; marsagliTime = []; }}>RESTART</button></div>

          <Plot
            data={[
              {
                x: Object.keys(histogramBox).map(key => key / bucketSize[1]),
                y: Object.keys(histogramBox).map(key => histogramBox[key]),
                type: 'bar'
              }
            ]}
            layout={{
              title: 'Przekształcenie Boxa i Mullera',
              autosize: false,
              width: 500,
              height: 500,

            }}
          />
        </span>
      </span>
      <span style={{ display: 'inline-block' }}>
        <span style={{ display: 'inline-block' }}>
          <div><button onClick={() => { setCounter(0); boxTime = []; marsagliTime = []; }}>RESTART</button></div>
          <Plot
            data={[
              {
                x: rozkladRownomiernyWIekszy.map(tuple => tuple[1]).filter((tuple,i) => i < stepRozklad * counter),
                y: rozkladRownomiernyWIekszy.map(tuple => tuple[2]).filter((tuple,i) => i < stepRozklad * counter),
                mode: 'markers',
                type: 'scatter'
              }
            ]}
            layout={{
              title: 'Rozklad równomierny z lab 1 przeskalowany do -1,1',
              autosize: false,
              width: 500,
              height: 500,

            }}
          />
        </span>
        <span style={{ display: 'inline-block' }}>
          <div>
            <h4>Wielkość kieszonki histogramu</h4>
            <button onClick={() => setBucketSize([0.001,1000])}>0.001</button>
            <button onClick={() => setBucketSize([0.01,100])}>0.01</button>
            <button onClick={() => setBucketSize([0.1,10])}>0.1</button>
            <button onClick={() => setBucketSize([0.2,5])}>0.2</button>
            <button onClick={() => setBucketSize([0.5,2])}>0.5</button>
            <button onClick={() => setBucketSize([1,1])}>1</button>
            <h4>Czas wtkonania algorytmu {(t3 - t2).toFixed(4)} ms <br />dla {(stepRozklad * counter).toFixed(0)} elementów </h4>
          </div>
          <div><button onClick={() => { setCounter(0); boxTime = []; marsagliTime = []; }}>RESTART</button></div>

          <Plot
            data={[
              {
                x: Object.keys(histogramMarzagli).map(key => key / bucketSize[1]),
                y: Object.keys(histogramMarzagli).map(key => histogramMarzagli[key]),
                type: 'bar'
              }
            ]}
            layout={{
              title: 'Algorytm Marsaglii i Braya',
              autosize: false,
              width: 500,
              height: 500,

            }}
          />
        </span>
      </span>

      <span style={{ display: 'inline-block' }}>

        <span style={{ display: 'inline-block' }}>

          <Plot
            data={[
              {
                x: boxTime.map((a,i) => (i).toFixed(0)),
                y: boxTime,
                type: 'scatter',
                name: 'Boxa i Mullera'
              },
              {
                x: marsagliTime.map((a,i) => (i).toFixed(0)),
                y: marsagliTime,
                type: 'scatter',
                name: 'Marsaglii i Braya'
              }
            ]}
            layout={{
              title: 'Czasy przekształcenia/algorytmu [ms]',
              autosize: true,
              width: 500,
              height: 500,

            }}
          />
        </span>


      </span>
    </div >
  );
}

export default App;
